# README #

* Mobile app using ionic framework 2 ( Angular 2, sass, html)
* v1.0
* gets daily earthquake data


# SCREEN SHOTS 


# Daily quakes
![IMG_2239.PNG](https://bitbucket.org/repo/ekk9bGk/images/229381207-IMG_2239.PNG)

# Most Severe toggled 
![IMG_2240.PNG](https://bitbucket.org/repo/ekk9bGk/images/1458230810-IMG_2240.PNG)

# Detailed view when selected from list
![IMG_2241.jpg](https://bitbucket.org/repo/ekk9bGk/images/2874924353-IMG_2241.jpg)