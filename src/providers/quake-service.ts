import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';

/*
  Generated class for the QuakeService provider.
  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/

@Injectable()
export class QuakeService {


  private baseUrl = 'https://earthquake.usgs.gov/earthquakes/feed/v1.0/summary/all_day.geojson';
  currentQuake: any = {};

  constructor(public http: Http) { }
   getQuakeData() : Observable<any> {
      return this.http.get(this.baseUrl)
      .map((response: Response) => {
         this.currentQuake = response.json();
         return this.currentQuake; 
      });     
  }
  
}




