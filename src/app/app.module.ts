import { NgModule, ErrorHandler } from '@angular/core';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { QuakeDetailPage } from '../pages/quake-detail-page/quake-detail-page';
import { QuakeService } from '../providers/quake-service';
import { AgmCoreModule } from 'angular2-google-maps/core';
import { Vibration, Shake } from 'ionic-native'; 

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    QuakeDetailPage    
  ],
  imports: [
    IonicModule.forRoot(MyApp),
    AgmCoreModule.forRoot({ apiKey: 'AIzaSyC0U4l-dMct0_loZRikPLXFe0gFTMk-amY'})

  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    QuakeDetailPage
  ],
  providers: [{provide: ErrorHandler, useClass: IonicErrorHandler}, QuakeService, Vibration, Shake]
})
export class AppModule {}
