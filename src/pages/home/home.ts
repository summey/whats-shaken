import { Component } from '@angular/core';
import { LoadingController, NavController, NavParams } from 'ionic-angular';
import { QuakeService } from '../../providers/quake-service';
import { QuakeDetailPage } from '../quake-detail-page/quake-detail-page';
import { Shake } from 'ionic-native';

import * as _ from 'lodash';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
  providers: [QuakeService]

})

export class HomePage {

  quakes: any[];
  magnitude: any[];
  magFilter = false;

  constructor(public navCtrl: NavController,
    private navParams: NavParams,
    private quakeService: QuakeService,
    private loadingController: LoadingController) {

  }
  //everything is working feb 25 2017
  magChanged() {

    if (this.magFilter != false) {
      this.magnitude = _.filter(this.quakes, function (o) {
        return o.properties.mag >= 4;
      });

      this.mostSevere();
    } else {
      this.shakeLoad();
    }

  }


  mostSevere() {

    this.quakes = this.magnitude;
    console.log(this.magnitude);

  }


  itemTapped($event, quake) {
    this.navCtrl.push(QuakeDetailPage, quake);
  }

  shakeLoad() {

    let loader = this.loadingController.create({
      content: "Feeling what's shaken'"
    });

    loader.present().then(() => {
      this.quakeService.getQuakeData().subscribe(
        (data) => {
          this.quakes = data.features;
          loader.dismiss();
          console.log(data.features);

        },
        (err) => {
          console.log(err);
        });
    });

  }

  ionViewDidLoad() {
    this.shakeLoad();
  }

  clearQuakeList(): void {
    this.quakes = [];
  }

  ionViewDidEnter() {
    let watch = Shake.startWatch(60).subscribe(() => {
      this.clearQuakeList();
      this.shakeLoad();
    });

    //watch.unsubscribe();
  }

}
