import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { Vibration } from 'ionic-native';

/*
  Generated class for the QuakeDetailPage page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/

@Component({
  selector: 'page-quake-detail',
  templateUrl: 'quake-detail-page.html'
})

export class QuakeDetailPage {
   
    quake: any;
    lat: number;
    lng: number;
    feel: number;

  constructor(public navCtrl: NavController, 
              public navParams: NavParams,
              public vibrate: Vibration) { 

                 this.quake = this.navParams.data;
                 this.lat = this.navParams.data.geometry.coordinates[1];
                 this.lng = this.navParams.data.geometry.coordinates[0];
                
                  if(this.navParams.data.properties.felt === null){
                    this.feel = 0;
                  }else{
                    this.feel = this.navParams.data.properties.felt;
                  }

                 //console.log(this.navParams.data.properties.felt);
                 //console.log('nav params', this.navParams);
              }

  ionViewDidLoad() {
      Vibration.vibrate(1000);
      

    
  }

}

